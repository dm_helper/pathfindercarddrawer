import React from 'react';
import DeckCover from '../Deck/DeckCover'
const deck = (props) => {
    let height = {
        height:'300px'
    }
    if(props.enlargeCard)
    {
        height = {
            height:'450px'
        }
    }
    else{
        height = {
            height:'300px'
        }
    }

    return(
        <div>
            
                
            <div>
                <div style={{display:'inline-block'}}>
                    <p>{props.deckType} deck</p>
                    <DeckCover deckType={props.deckType} cardToDrawCount={props.cardToDrawCount}/>
                </div>
                <div style={{display:'inline-block'}}>
                <p>Current drawn card</p>
                <img id={"success" + props.cardToDrawCount}onClick={props.enlargeCardAction} style={height} src={props.deckArray[props.cardToDrawCount]}/>
                </div>
                
            </div>
            <button className="btn btn-primary" onClick={props.shuffleDeck}>Shuffle</button>
            <button className="btn btn-success" onClick={props.drawACard}>Draw a card</button>
            {props.cardToDrawCount <1?
            <button className="btn btn-info" onClick={props.openModal} disabled>Discarded cards</button>
            :<button className="btn btn-info" onClick={props.openModal}>Discarded cards</button>}
                
            
            
           
        </div>
    );
}
export default deck;