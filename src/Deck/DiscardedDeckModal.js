import React from 'react';
import '.././App.css';

const discardedDeckModal = (props) => {
  return (
    <div id={props.modalId} className="myModal displayNone">
      <div className="myModalContent">
        <div style={{height:'3em'}}>
          <span onClick={props.openModalHandler} className="closeModal">&times;</span>
        </div>

        <div>
          {props.discardedDeckArray}
        </div>
      </div>
    </div>
  )
}

export default discardedDeckModal;