import React from 'react';
import SuccesCover from '../CritDeckImages/Cover.PNG'
import SuccesCoverNoCard from '../CritDeckImages/CoverNoCards.PNG'
import FumbleCover from '../FumbleDeckImages/Cover.PNG'
import FumbleCoverNoCard from '../FumbleDeckImages/CoverNoCards.PNG'

const critDeckCover = (props) =>{
    let imgSrc = null;
    let imgAlt = null;
    if(props.deckType === "Fumble"){
        imgAlt = "Fubmle Deck cover";
        if(props.cardToDrawCount > 52){
            imgSrc = FumbleCoverNoCard;
        }else{
            imgSrc = FumbleCover;          
        }
    }
    else{
        imgAlt = "Success Deck cover";
        if(props.cardToDrawCount > 52){
            imgSrc = SuccesCoverNoCard;
        }else{
            imgSrc = SuccesCover;          
        }

    }
    return(
        <img id="tk" style={{height:'300px'}} src={imgSrc} alt={imgAlt}></img>
    )
}

export default critDeckCover;