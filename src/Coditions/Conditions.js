import blinded from '../ConditionImages/Blinded.PNG'
import broken from '../ConditionImages/Broken.PNG'
import clumsy from '../ConditionImages/Clumsy.PNG'
import concealed from '../ConditionImages/Concealed.PNG'
import confused from '../ConditionImages/Confused.PNG'
import controled from '../ConditionImages/Controled.PNG'
import dazzled from '../ConditionImages/Dazzled.PNG'
import deafened from '../ConditionImages/Deafened.PNG'
import doomed from '../ConditionImages/Doomed.PNG'
import drained from '../ConditionImages/Drained.PNG'
import dying from '../ConditionImages/Dying.PNG'
import enfeebled from '../ConditionImages/Enfeebled.PNG'
import fascinated from '../ConditionImages/Fascinated.PNG'
import fatigued from '../ConditionImages/Fatigued.PNG'
import flatFooted from '../ConditionImages/FlatFooted.PNG'
import fleeing from '../ConditionImages/Fleeing.PNG'
import friendly from '../ConditionImages/Friendly.PNG'
import frightened from '../ConditionImages/Frightened.PNG'
import grabbed from '../ConditionImages/Grabbed.PNG'
import helpful from '../ConditionImages/Helpful.PNG'
import hidden from '../ConditionImages/Hidden.PNG'
import hostile from '../ConditionImages/Hostile.PNG'
import immobilized from '../ConditionImages/Immobilized.PNG'
import indifferent from '../ConditionImages/Indifferent.PNG'
import invisible from '../ConditionImages/Invisible.PNG'
import observed from '../ConditionImages/Observed.PNG'
import paralyzed from '../ConditionImages/Paralyzed.PNG'
import persistentDamage from '../ConditionImages/PersistentDamage.PNG'
import petrified from '../ConditionImages/Petrified.PNG'
import prone from '../ConditionImages/Prone.PNG'
import quickened from '../ConditionImages/Quickened.PNG'
import restrained from '../ConditionImages/Restrained.PNG'
import sickened from '../ConditionImages/Sickened.PNG'
import slowed from '../ConditionImages/Slowed.PNG'
import stunned from '../ConditionImages/Stunned.PNG'
import stupified from '../ConditionImages/Stupified.PNG'
import unconscious from '../ConditionImages/Unconscious.PNG'
import undetected from '../ConditionImages/Undetected.PNG'
import unfriendly from '../ConditionImages/Unfriendly.PNG'
import unnoticed from '../ConditionImages/Unnoticed.PNG'
import wounded from '../ConditionImages/Wounded.PNG'


const blindedName = "Blinded";
const brokenName = "Broken";
const clumsyName = "Clumsy";
const concealedName = "Concealed";
const confusedName = "Confused";
const controledName = "Controled";
const dazzledName = "Dazzled";
const deafenedName = "Deafened";
const doomedName = "Doomed";
const drainedName = "Drained";
const dyingName = "Dying";
const enfeebledName = "Enfeebled";
const fascinatedName = "Fascinated";
const fatiguedName = "Fatigued";
const flatFootedName = "Flat-Footed";
const fleeingName = "Fleeing";
const friendlyName = "Friendly";
const frightenedName = "Frightened";
const grabbedName = "Grabbed";
const helpfulName = "Helpful";
const hiddenName = "Hidden";
const hostileName = "Hostile"
const immobilizedName = "Immobilized";
const indifferentName = "Indifferent";
const invisibleName = "Invisible";
const observedName = "Observed";
const paralyzedName = "Paralyzed";
const persistentDamageName = "Persistent Damage";
const petrifiedName = "Petrified";
const proneName = "Prone";
const quickenedName = "Quickened";
const restrainedName = "Restrained";
const sickenedName = "Sickened";
const slowedName = "Slowed";
const stunnedName = "Stunned";
const stupifiedName = "Stupified";
const unconsciousName = "Unconscious";
const undetectedName = "Undetected";
const unfriendlyName = "Unfriendly";
const unnoticedName = "Unnoticed";
const woundedName = "Wounded";


const conditionNames = [
    blindedName,
    brokenName,
    clumsyName,
    concealedName,
    confusedName,
    controledName,
    dazzledName,
    deafenedName,
    doomedName,
    drainedName,
    dyingName,
    enfeebledName,
    fascinatedName,
    fatiguedName,
    flatFootedName,
    fleeingName,
    friendlyName,
    frightenedName,
    grabbedName,
    helpfulName,
    hiddenName,
    hostileName,
    immobilizedName,
    indifferentName,
    invisibleName,
    observedName,
    paralyzedName,
    persistentDamageName,
    petrifiedName,
    proneName,
    quickenedName,
    restrainedName,
    sickenedName,
    slowedName,
    stunnedName,
    stupifiedName,
    unconsciousName,
    undetectedName,
    unfriendlyName,
    unnoticedName,
    woundedName
];

const conditionSrc = [
    blinded,
    broken,
    clumsy,
    concealed,
    confused,
    controled,
    dazzled,
    deafened,
    doomed,
    drained,
    dying,
    enfeebled,
    fascinated,
    fatigued,
    flatFooted,
    fleeing,
    friendly,
    frightened,
    grabbed,
    helpful,
    hidden,
    hostile,
    immobilized,
    indifferent,
    invisible,
    observed,
    paralyzed,
    persistentDamage,
    petrified,
    prone,
    quickened,
    restrained,
    sickened,
    slowed,
    stunned,
    stupified,
    unconscious,
    undetected,
    unfriendly,
    unnoticed,
    wounded
];

const conditions = () =>{
   let  conditionObjet =[];
   for(let i = 0; i<conditionNames.length; i++){
       conditionObjet.push( {
        Name: conditionNames[i],
        ImageSrc: conditionSrc[i],
        key: i
           })
   };
    return conditionObjet;
}

export default conditions;





