import React, { Component } from 'react';
import './App.css';
import CritDeckArray from './CritDeck/CritDeckImageArray';
import FumbleDeckArray from './FumbleDeck/FumbleDeckImageArray';
import Deck from './Deck/Deck'
import DiscardedDeckModal from './Deck/DiscardedDeckModal';
import Conditions from './Coditions/Conditions'


const successId ="Success_Modal";
const failureId ="Failure_Modal";
class App extends Component {
  state = {
    enlargeErrorCard: false,
    enlargedSuccessCard:false,
    drawCritCount: -1,
    shuffledCritDeckArray: [...CritDeckArray()],
    discardedCritDeckArray: [],
    openSuccesModal: false,
    drawFumbleCount: -1,
    shuffledFumbleDeckArray:[...FumbleDeckArray()],
    discardedFumbleDeckArray: [],
    openFumbleModal: false,
    conditionToShowSrc: null
  }

  enlargeSuccessCardHandler = () => {
    let shouldEnlargeCard = !this.state.enlargedSuccessCard;
    this.setState({ enlargedSuccessCard: shouldEnlargeCard })
  }

  enlargeErrorCardHandler = () => {
    let shouldEnlargeCard = !this.state.enlargeErrorCard;
    this.setState({ enlargeErrorCard: shouldEnlargeCard })
  }

  drawCritSuccesHandler = () => {
    this.setState((prevState, props) => {
      const discardDeck = [];
      for (let index = 0; index <= this.state.drawCritCount; index++) {
        discardDeck.push(this.state.shuffledCritDeckArray[index])

      }
      return {
        drawCritCount: prevState.drawCritCount + 1,
        discardedCritDeckArray: discardDeck
      }
    });
  }

  drawFumbleHandler = () => {
    this.setState((prevState, props) => {
      const discardDeck = [];
      for (let index = 0; index <= this.state.drawFumbleCount; index++) {
        discardDeck.push(this.state.shuffledFumbleDeckArray[index])

      }
      return {
        drawFumbleCount: prevState.drawFumbleCount + 1,
        discardedFumbleDeckArray: discardDeck
      }
    });
  }

  shuffleCritSuccessDeckHandler = () => {
    const shuffledCritDeckArray = this.shuffleArray(CritDeckArray());
    this.setState({
      shuffledCritDeckArray: shuffledCritDeckArray,
      drawCritCount: -1
    })
  }

  shuffleFumbleDeckHandler = () => {
    const shuffledFumbleDeckArray = this.shuffleArray(FumbleDeckArray());
    this.setState({
      shuffledFumbleDeckArray: shuffledFumbleDeckArray,
      drawFumbleCount: -1
    })
  }

  openSuccessModalHandler = () => {
    const modalState = !this.state.openSuccesModal;
    this.setState({ openSuccesModal: modalState });
    this.handleModalDisplay(modalState, successId);
  }

  openFumbleModalHandler = () => {
    const modalState = !this.state.openFumbleModal;
    this.setState({ openFumbleModal: modalState });
    this.handleModalDisplay(modalState, failureId);
  }

  showConditionHandler = (id) =>{
    const condtitionSrc = Conditions()[id].ImageSrc;
    this.setState({conditionToShowSrc: condtitionSrc});
  }

  closeConditionHandler = () =>{
    this.setState({conditionToShowSrc: null});
  }

  handleModalDisplay =(shouldDisplay, modalId) =>{
    if (shouldDisplay) {
      document.getElementById(modalId).classList.remove("displayNone")
      document.getElementById(modalId).classList.add("displayBlock")
     }
     else {
        document.getElementById(modalId).classList.remove("displayBlock")
        document.getElementById(modalId).classList.add("displayNone")
     }
  }

  shuffleArray = (array) => {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }
  render() {
    let discardedSuccesCards = [];
    if (this.state.openSuccesModal) {
      if (this.state.drawCritCount > 0) {
        for (let index = this.state.discardedCritDeckArray.length; index >= 0; index--) {
          let discardedCard = <img key={index} src={this.state.discardedCritDeckArray[index]}></img>;
          discardedSuccesCards.push(discardedCard);       
        }      
      }
    }

    let discardedFumbleCards = [];
    if (this.state.openFumbleModal) {
      if (this.state.drawFumbleCount > 0) {
        for (let index = this.state.discardedFumbleDeckArray.length; index >= 0; index--) {
          let discardedCard = <img style={{height:'450px'}}key={index} src={this.state.discardedFumbleDeckArray[index]}></img>;
          discardedFumbleCards.push(discardedCard);       
        }      
      }
    }

    let ConditionsElement = [];
    let conditionsObject = Conditions();
    conditionsObject.forEach(element => {
      let condtition =
        <div key={element.key}style={{ border: 'double', marginBottom: '1em' }}>
          <div style={{ fontWeight: 'bold' }} onClick={() => this.showConditionHandler(element.key)}>{element.Name}</div>
        </div>;
      ConditionsElement.push(condtition);
    });

    let ConditionToDisplay = null;
    if (this.state.conditionToShowSrc !== null) {
      ConditionToDisplay = 
      <div className="myModal" >
        <img className="myConditionDisplay displayBlock" src={this.state.conditionToShowSrc}></img>
        <button className="btn btn-warning" onClick={this.closeConditionHandler}>Close</button> 
      </div>       
    }
    else{
      ConditionToDisplay = null;
    }

    return (
      <div className="App">
        <div className="appContainer">
          <div className="row">
            <div className="col-2" style={{ height: '70em' , overflowY: 'scroll'}}>{ConditionsElement}</div>
            <div className="col-10">
              <div className="Header"> This is Pathfinder 2e critical success card drawer</div>

              <Deck
                deckType="Success"
                deckArray={this.state.shuffledCritDeckArray}
                cardToDrawCount={this.state.drawCritCount}
                drawACard={this.drawCritSuccesHandler}
                shuffleDeck={this.shuffleCritSuccessDeckHandler}
                enlargeCardAction={this.enlargeSuccessCardHandler}
                enlargeCard={this.state.enlargedSuccessCard}
                openModal={this.openSuccessModalHandler}
              />
              <DiscardedDeckModal
                modalId={successId}
                openModalHandler={this.openSuccessModalHandler}
                discardedDeckArray={discardedSuccesCards} />

              {ConditionToDisplay}
              <p></p>
              <div className="Header"> This is Pathfinder 2e critical fumble card drawer</div>

              <Deck
                deckType="Fumble"
                deckArray={this.state.shuffledFumbleDeckArray}
                cardToDrawCount={this.state.drawFumbleCount}
                drawACard={this.drawFumbleHandler}
                shuffleDeck={this.shuffleFumbleDeckHandler}
                enlargeCardAction={this.enlargeErrorCardHandler}
                enlargeCard={this.state.enlargeErrorCard}
                openModal={this.openFumbleModalHandler}
              />
              <DiscardedDeckModal
                modalId={failureId}
                openModalHandler={this.openFumbleModalHandler}
                discardedDeckArray={discardedFumbleCards} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
