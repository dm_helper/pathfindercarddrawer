const critDeckArray = () =>{
    const imgArray = [];
    for (let i = 1; i <= 53; i++) {
        const img = require(`../FumbleDeckImages/Screenshot_${i}.png`);
        imgArray.push(img);
    }
    return imgArray;
}

export default critDeckArray;